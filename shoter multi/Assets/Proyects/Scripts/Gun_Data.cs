using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "New Gun", menuName = "Gun")]
public class Gun_Data : ScriptableObject
{
    #region variables

    public string gunName;
    public int maxAmmoCount;
    public int balasActual;
    public float reloadTime;
    public float damage;
    public float fireRate;
    public float range;
    public Vector2 recoil;
    public Vector3 Offset;

    public bool automatic;

    #endregion

}
