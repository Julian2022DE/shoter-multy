using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class GunController : MonoBehaviour
{
    #region variables

    public VisualEffect ammoShoot;
    public Gun[] guns;
    public Gun actualGun;
    public int indexGun = 0;
    public int maxGuns = 3;


    float lastShotTime = 0f;
    Vector3 currentRotacion;
    Vector3 targetRotation;
    public float returnSeed;
    public float snapinnes;
    float lastReload;
    bool reloading;

    bool isChaning;
    float changTime;
    float lastchaingTime;

    public GameObject prefBuller;

   
    public Player_controller playerController;

    public float distance;
    public LayerMask lm;
    #endregion

    #region Unity Funtions
    private void Update()
    {
        if(Input.GetButton("Take") && Physics.Raycast(playerController.cam.transform.position, playerController.cam.transform.forward, out RaycastHit hit, distance, lm))
        {
            Transform gun = hit.transform;
            if(hit.transform != null)
            {
                Interacciones(gun, indexGun);
                
            }
        }
        if (actualGun != null)
        {
            if (lastShotTime <= 0)
            {
                if (!actualGun.data.automatic)
                {
                    if (Input.GetButtonDown("Fire1"))
                    {
                        if (actualGun.data.balasActual > 0)
                        {
                            Shoot();
                        }
                    }
                }
                else
                {
                    if (Input.GetButton("Fire1"))
                    {
                        if (actualGun.data.balasActual > 0)
                        {
                            Shoot();
                        }
                    }
                }
            }
            if (Input.GetButtonDown("Reload") && !reloading)
            {
                if (actualGun.data.balasActual < actualGun.data.maxAmmoCount)
                {
                    lastReload = 0;
                    reloading = true;
                    actualGun.weaponShoot.PlayOneShot(actualGun.recarga);
                }
            }
        }
        else
        {
            if (actualGun == null)
            {

            }
        }

        if (lastShotTime >= 0)
        {

            lastShotTime -= Time.deltaTime;
        }
        if (reloading)
        {
            lastReload += Time.deltaTime;
            if (lastReload >= actualGun.data.reloadTime)
            {
                reloading = false;
                Reload();
            }
        }
        targetRotation = Vector3.Lerp(targetRotation, Vector3.zero, returnSeed * Time.deltaTime);
        currentRotacion = Vector3.Slerp(currentRotacion, targetRotation, snapinnes * Time.deltaTime);
        playerController.recoil.localRotation = Quaternion.Euler(currentRotacion);

        if (Input.GetButtonDown("Gun1"))
        {
            if (indexGun != 0)
            {
                indexGun = 0;
                lastchaingTime = 0;
                if (actualGun != null)
                {
                    actualGun.weaponShoot.PlayOneShot(actualGun.cambioDeArma);
                    actualGun.gameObject.SetActive(false);
                    actualGun = null;
                }
                isChaning = true;
            }
        }
        if (Input.GetButtonDown("Gun2"))
        {
            if (indexGun != 1)
            {
                indexGun = 1;
                lastchaingTime = 0;
                if (actualGun != null)
                {
                    actualGun.weaponShoot.PlayOneShot(actualGun.cambioDeArma);
                    actualGun.gameObject.SetActive(false);
                    actualGun = null;
                }
                isChaning = true;

            }
        }
        if (Input.GetButtonDown("Gun3"))
        {
            if (indexGun != 2)
            {

                indexGun = 2;
                lastchaingTime = 0;
                if (actualGun != null)
                {
                    actualGun.weaponShoot.PlayOneShot(actualGun.cambioDeArma);
                    actualGun.gameObject.SetActive(false);
                    actualGun = null;
                }
                isChaning = true;

            }
        }

        if(isChaning)
        {
            lastchaingTime += Time.deltaTime;
            if(lastchaingTime >= changTime)
            {
                isChaning = false;
                changeGun(indexGun);
            }
        }   
    }


    #endregion 

    #region Personal Funtions
    private void Shoot()
    {
        
        if (reloading == false)
        {
            if (Physics.Raycast(playerController.cam.transform.position, playerController.cam.transform.forward, out RaycastHit hit, actualGun.data.range))
            {
                if (hit.transform != null)
                {
                    Debug.Log($"we shoting at: {hit.transform.name}");
                    GameObject pref = Instantiate(prefBuller, hit.point + hit.normal * 0.001f, Quaternion.LookRotation(hit.normal, Vector3.up));
                    Destroy(pref, 5f);
                                
                }
            }
            actualGun.data.balasActual--;
            lastShotTime = actualGun.data.fireRate;
            actualGun.weaponShoot.PlayOneShot(actualGun.Sonido);
            addrecoil();
            GameObject go = VisualEffect.Instantiate(ammoShoot, actualGun.muzzlePoint.position, actualGun.muzzlePoint.rotation).gameObject;
            go.transform.parent = actualGun.muzzlePoint;
            Destroy(go, 2f);

        }      
    }   

    void addrecoil()
    {
        currentRotacion -= new Vector3(actualGun.data.recoil.x, Random.Range(-actualGun.data.recoil.y, actualGun.data.recoil.y), 0f);
      
    }
    void Reload()
    {
        actualGun.data.balasActual = actualGun.data.maxAmmoCount;
    }

    void changeGun(int index)
    {
        
         if(guns[index] != null)
         {
            actualGun = guns[index];
            actualGun.gameObject.SetActive(true);
         }
    }
    void Interacciones(Transform floor_gun, int index)
    {
        if(actualGun != null)
        {
            RealeasedGun(actualGun, indexGun);
        }
        Rigidbody rb = floor_gun.GetComponent<Rigidbody>();
        rb.isKinematic = true;
        actualGun = floor_gun.transform.gameObject.GetComponent<Gun>();
        floor_gun.transform.localPosition = actualGun.data.Offset;
        floor_gun.transform.localRotation = Quaternion.identity;
        guns[index] = actualGun;
    }

    public void RealeasedGun(Gun floor_Gun, int Index)
    {
        Rigidbody rb = floor_Gun.GetComponent<Rigidbody>();
        actualGun.transform.parent = null;
        guns[Index] = null;
        actualGun = null;
        rb.isKinematic = false;
        rb.AddForce(floor_Gun.transform.up * 2, ForceMode.Impulse);
        rb.AddTorque(floor_Gun.transform.right * 2, ForceMode.Impulse);
    }
    #endregion
}
