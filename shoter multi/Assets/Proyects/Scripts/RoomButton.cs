using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using TMPro;
using Photon.Realtime;


public class RoomButton : MonoBehaviour
{
    [SerializeField] private TMP_Text nombre_Botones;
    [SerializeField] private RoomInfo roomInfo;

    public void SetButtonDetails(RoomInfo inputInfo)
    {
        roomInfo = inputInfo;
        nombre_Botones.text = roomInfo.Name;
    }
}


