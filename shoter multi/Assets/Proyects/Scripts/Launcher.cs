using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using TMPro;
using Photon.Realtime;

public class Launcher : MonoBehaviourPunCallbacks
{
    #region
    public static Launcher Instance;
    [SerializeField] private GameObject[] screenGameobject;
    [SerializeField] TMP_Text infoText;
    [SerializeField] TMP_Text NameRoom;
    [SerializeField] TMP_Text errorText;
    [SerializeField] TMP_InputField roonNameInput;
    [SerializeField] GameObject prefButton;
    [SerializeField] List <RoomButton> butons = new List<RoomButton>();
    [SerializeField] Transform contentScroll;

    #endregion

    #region
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }
    #endregion
    private void Start()
    {
        infoText.text = "conecting to network";
        SetScreenObjects(0);
        PhotonNetwork.ConnectUsingSettings();

    }
    public override void OnConnectedToMaster()
    {
        PhotonNetwork.JoinLobby();
        SetScreenObjects(1);
    }
    #region
    public void SetScreenObjects(int index)
    {
        for (int i = 0; i < screenGameobject.Length; i++)
        {
            screenGameobject[i].SetActive(i == index);
        }
    }

    public void createRoom()
    {
        if (!string.IsNullOrEmpty(roonNameInput.text))
        {
            RoomOptions options = new RoomOptions();
            options.MaxPlayers = 10;
            PhotonNetwork.CreateRoom(roonNameInput.text);
            infoText.text = "creating Room...";
            SetScreenObjects(0);
        }
    }

    public override void OnJoinedRoom()
    {
        NameRoom.text = PhotonNetwork.CurrentRoom.Name;
        SetScreenObjects(3);
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        errorText.text = "Te jodiste crack ya existo";
        SetScreenObjects(4);
    }
    public void Back()
    {
        PhotonNetwork.LeaveRoom();
        SetScreenObjects(1);
    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        for (int i = 0; i < roomList.Count; i++)
        {
            if(roomList[i].PlayerCount != roomList[i].MaxPlayers && !roomList[i].RemovedFromList)
            {
                RoomButton newRoomButton = Instantiate(prefButton, contentScroll).GetComponent<RoomButton>();
                newRoomButton.SetButtonDetails(roomList[i]);
                butons.Add(newRoomButton);
            }
            if (roomList[i].RemovedFromList)
            {
                Debug.Log(roomList[i].Name);
                butons.Remove(butons[i]);
                Destroy(butons[i]);
            }
        }
    }

    public void JoinRoom(RoomInfo info)
    {
        PhotonNetwork.JoinRoom(info.Name);
        infoText.text = "Joining Room";
        SetScreenObjects(index: 0);
    }

    #endregion
}
