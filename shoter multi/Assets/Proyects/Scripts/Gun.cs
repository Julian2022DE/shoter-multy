using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    #region variables

    public Gun_Data data;
    public Transform muzzlePoint;
    public AudioSource weaponShoot;
    public AudioClip Sonido;
    public AudioClip recarga;
    public AudioClip cambioDeArma;


    #endregion

    #region unityFuntions
    private void Awake()
    {
        data.balasActual = data.maxAmmoCount;
    }
    #endregion
}
