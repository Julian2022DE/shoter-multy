using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_controller : MonoBehaviour
{

    #region Variables

    public Camera cam;
    public Transform recoil;
    public Transform gunpoitn;
    [SerializeField] private CharacterController charactercontroller;
    [SerializeField] private Transform pointOfView;
    [SerializeField] private float walkspeed = 0f;
    [SerializeField] private float runSpeed = 0f;
    [SerializeField] private float jumpForce = 0f;
    [SerializeField] private float gravityMode = 0f;

    private float actualSpeed;
    private float horizontalRotationStore;
    private float verticalRotationStore;
    
    public Vector2 mouseinput;
    public Vector3 direction;
    public Vector3 Movement;

    [Header("Ground Direction")]
    [SerializeField] private bool isGround;
    [SerializeField] private float radio;
    [SerializeField] private float distance;
    [SerializeField] private Vector3 offset;
    [SerializeField] private LayerMask lm;

    public Launcher launcher;
    #endregion

    #region Unity Funtions

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        cam = Camera.main;
    }

    private void LateUpdate()
    {
        cam.transform.position = recoil.position;
        cam.transform.rotation = recoil.rotation;

        gunpoitn.transform.position = recoil.position;
        gunpoitn.transform.rotation = recoil.rotation;
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position + offset, radio);
        if (Physics.SphereCast(transform.position + offset, radio, Vector3.down, out RaycastHit Hit, distance, lm))
        {
            Gizmos.color = Color.green;
            Vector3 endpoint = (transform.position + offset) + (Vector3.down * distance);
            Gizmos.DrawWireSphere(endpoint, radio);
            Gizmos.DrawLine(transform.position + offset, endpoint);

            Gizmos.DrawSphere(Hit.point, 0.1f);
        }
        else
        {
            Gizmos.color = Color.red;
            Vector3 endpoint = (transform.position + offset) + (Vector3.down * distance);
            Gizmos.DrawWireSphere(endpoint, radio);
            Gizmos.DrawLine(transform.position + offset, endpoint);
        }
    }
    private void Update()
    {
        Rotation();
        MovementController();
    }
    #endregion

    #region
    public void Rotation()
    {
        mouseinput = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));

        horizontalRotationStore += mouseinput.x;
        verticalRotationStore -= mouseinput.y;

        verticalRotationStore = Mathf.Clamp(verticalRotationStore, -60, 60);
        transform.rotation = Quaternion.Euler(0f ,horizontalRotationStore, 0f);
        pointOfView.transform.localRotation = Quaternion.Euler(verticalRotationStore, transform.rotation.y, 0f);
    }

    public void MovementController()
    {
        direction = new Vector3(Input.GetAxisRaw("Horizontal"), 0f, Input.GetAxisRaw("Vertical"));

        float VelY = Movement.y;
        Movement = ((transform.forward * direction.z) + (transform.right * direction.x)).normalized;
        Movement.y = VelY;

        if(Input.GetButton("Fire3"))
        {
            actualSpeed = runSpeed;
        }
        else
        {
            actualSpeed = walkspeed;
        }
        if(Isgrounded())
        {
            Movement.y = 0;
        }
        if(Input.GetButtonDown("Jump") && Isgrounded())
        {
            Movement.y = jumpForce * Time.deltaTime;
        }
        Movement.y += Physics.gravity.y * Time.deltaTime * gravityMode;
        charactercontroller.Move( Movement * (actualSpeed * Time.deltaTime));
    }

    private bool Isgrounded()
    {
        isGround = false;
        if(Physics.SphereCast(transform.position + offset, radio, Vector3.down, out RaycastHit Hit, distance, lm))
        {
            isGround = true;
        }
        return isGround;
    }
    #endregion
}
